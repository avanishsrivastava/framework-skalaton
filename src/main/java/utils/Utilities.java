package utils;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class Utilities extends SeleniumActions {

    /**
     * Method Name: launchBrowser
     * Parameters: browserName as String
     * Return Type: None
     * Exceptions: None
     * Objective: To launch the browser before test suite execution
     **/

    @BeforeSuite
    public void launchBrowser() {
        String browserName = getPropertyValue(configPropertiesFile, "browserName");
        openBrowser(browserName);
    }

    /**
     * Method Name: trashBrowser
     * Parameters: None
     * Return Type: None
     * Exceptions: None
     * Objective: To close the browser after test suite execution
     **/

    @AfterSuite
    public void trashBrowser() {
        closeBrowser();
    }

    /**
     * Method Name: login
     * Parameters: userName as String and password as String
     * Return Type: None
     * Exceptions: None
     * Objective: To login the user with provided userName and password
     **/

    public void login(String userName, String password) {

    }

    /**
     * Method Name: logout
     * Parameters: None
     * Return Type: None
     * Exceptions: None
     * Objective: To logout the user from application
     **/

    public void logout() {

    }

}